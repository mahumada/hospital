# hospital by Manuel Ahumada

To create your mysql database type this on your Linux command line (terminal):

```
$ sudo mysql demodb < database.sql
```

Make sure to have a database called demodb. The user that will enter is 'root' with no password. Server used is in localhost.
