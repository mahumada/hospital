const e = require('express');
const express = require('express');
const app = express();

const mysql = require('mysql2');

app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static('frontFiles'))


const sql = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'demodb'
});


// Home page
app.get('/', (req, res)=>{
    return res.status(200).send('<h1>Hospital API by Manuel Ahumada</h1><h3>Go to</h3><nav><ul><li><a href="/patient">Patients</a></li><li><a href="/doctor">Doctors</a></li><li><a href="/room">Rooms</a></li><li><a href="/field">Fields</a></li><li><a href="/zone">Zones</a></li><li><a href="/room">Rooms</a></li><li><a href="/operating_room">Operating Rooms</a></li><li><a href="/works_with">Works with</a></li><li><a href="/relations">Doctor to Patient relation</a></li><li><a href="/occupation">Room occupation info</a></li></nav>')
});


// Update OR Doctor Working value
app.put('/operating_room/:operating_room_id', (req, res)=>{
  sql.query('UPDATE operating_room SET doctor_working = ? WHERE operating_room_id = ?', [req.body.doctor_working, Number(req.params.operating_room_id)], (error, results, fields)=>{
    if(error){
      return res.status(404).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})


// Add Patient and Update Room Occupation
app.post('/patient', (req, res)=>{
  const { patient_id, first_name, last_name, start_date, needs, is_contagious, room_number } = req.body;
  sql.query('SELECT is_free FROM room WHERE room_id = ? AND is_free = 1;', room_number, (error, results, fields)=>{
    if(error || results[0] === undefined){
      return res.status(409).json({success: false, error: 'Room already in use...'});
    }else if(results[0].is_free === 1){
      sql.query('INSERT INTO patient VALUES(?, ?, ?, ?, ?, ?, ?);', [patient_id, first_name, last_name, start_date, needs, is_contagious, room_number], (error, results, fields)=>{
        if(error){
          console.log(error);
          return res.status(409).json(error);
        }else{
          sql.query('UPDATE room JOIN patient ON room.room_id = patient.room_number SET room.is_free = 0;', (error, results, fields)=>{
            if(error){
              return res.status(409).json(error);
            }else{
              sql.query('INSERT INTO patient_archive(first_name, last_name, start_date) VALUES(?, ?, ?);', [first_name, last_name, start_date], (error, results, fields)=>{
                if(error){
                  return res.status(409).json(error);
                }else{
                  return res.status(200).json(results);
                }
              })
            }   
          });
        }   
      });
    }else{
      return res.status(409).json({success: false, error: 'Room already occupied...'})
    }
  });
});

// Add Doctor
app.post('/doctor', (req, res)=>{
  const { doctor_id, first_name, last_name, field, salary, shift, start_date } = req.body;
  sql.query('INSERT INTO doctor VALUES(?, ?, ?, ?, ?, ?, ?);', [ doctor_id, first_name, last_name, field, salary, shift, start_date ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// Add Zone
app.post('/zone', (req, res)=>{
  const { zone_id, zone_name } = req.body;
  sql.query('INSERT INTO zone VALUES(?, ?);', [ zone_id, zone_name ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// Add Room
app.post('/room', (req, res)=>{
  const { room_id, zone } = req.body;
  sql.query('INSERT INTO room VALUES(?, ?, 1);', [ room_id, zone ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// Add OR
app.post('/operating_room', (req, res)=>{
  const { operating_room_id, zone_name } = req.body;
  sql.query('INSERT INTO operating_room VALUES(?, 1, ?, NULL);', [ operating_room_id, zone_name ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// Add Field
app.post('/field', (req, res)=>{
  const { field_id, field_name, budget, zone, attending } = req.body;
  sql.query('INSERT INTO field VALUES(?, ?, ?, ?, ?);', [ field_id, field_name, budget, zone, attending ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// Add Works_with
app.post('/works_with', (req, res)=>{
  const { patient_id, doctor_id, treatment } = req.body;
  sql.query('INSERT INTO field VALUES(?, ?, ?);', [ patient_id, doctor_id, treatment ], (error, results, fields)=>{
    if(error){
      return res.status(409).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})


// Delete Patient and Update Room Occupation
app.delete('/patient/:patient_id', (req, res)=>{
  sql.beginTransaction(err => {
    if(err){ 
      return res.status(404).json(err);
    } else {
      sql.query('UPDATE room JOIN patient SET room.is_free = 1 WHERE room.room_id = (SELECT patient.room_number FROM patient WHERE patient.patient_id = ?)', req.params.patient_id, (error, results, fields)=>{
        if(error){
          sql.rollback(()=>{
            throw err;
          });
          return res.status(404).json(error);
        }else{
          sql.query('SELECT first_name, last_name, start_date FROM patient WHERE patient_id = ?', req.params.patient_id, (error, results, fields)=>{
            if(error){
              return res.status(409).json(error);
            }else{
              console.log(results);
              const {first_name, last_name, start_date} = results[0];
              sql.query('DELETE FROM patient WHERE ? = patient_id', req.params.patient_id, (error, results, fields)=>{
                if(error){
                  sql.rollback(()=>{
                    throw err;
                  });
                  return res.status(404).json(error);
                }else{
                  const end_date = new Date().toISOString().slice(0, 10)+"T"+new Date().toLocaleTimeString('en-GB').toString();
                  sql.query('UPDATE patient_archive SET end_date = ? WHERE first_name = ? AND last_name = ? AND start_date = ?', [end_date, first_name, last_name, start_date], (error, results, fields)=>{
                    if(error){
                      sql.rollback(()=>{
                        throw err;
                      });
                      return res.status(409).json(error);
                    }else{
                      sql.commit(err => {
                        if(err){
                          sql.rollback(()=>{
                            throw err;
                          });
                        } else {
                          console.log('DELETE PATIENT Transaction commited!');
                          return res.status(200).json(results);
                        }
                      })
                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  })
  
  // Delete Doctor
  app.delete('/doctor/:doctor_id', (req, res)=>{
    sql.query('DELETE FROM doctor WHERE doctor_id = ?', req.body.doctor_id, (error, results, fields)=>{
      if(error){
        return res.status(404).json(error);
      }else{
        return res.status(200).json(results);
      }
    })
  })
})


// Relations query
app.get('/relations', (req, res)=>{
  sql.query('SELECT doctor.doctor_id, doctor.first_name AS doctor_first_name, doctor.last_name AS doctor_last_name, works_with.patient_id, patient.first_name AS patient_first_name, patient.last_name AS patient_last_name, works_with.treatment FROM doctor JOIN works_with ON works_with.doctor_id = doctor.doctor_id JOIN patient WHERE works_with.patient_id = patient.patient_id;', (error, results, fields)=>{
    if(error){
      return res.status(404).json(error);
    }else{
      return res.status(200).json(results);
    }
  });
});

// Room Occupation Query
app.get('/occupation', (req, res)=>{
  sql.query('SELECT room.room_id, patient.patient_id, patient.first_name, patient.last_name FROM patient JOIN room ON room.room_id = patient.room_number;', (error, results, fields)=>{
    if(error){
      return res.status(404).json(error);
    }else{
      return res.status(200).json(results);
    }
  });
});

// API Find Query
app.post('/finder', (req, res)=>{
  sql.query('SELECT patient_id FROM patient WHERE first_name = ? AND last_name = ?;', [req.body.first_name, req.body.last_name], (error, results, fields)=>{
    if(error){
      return res.status(404).json(error);
    }else{
      return res.status(200).json(results);
    }
  })
})

// API GET Query
app.get('/:routeName', (req, res)=>{
    for(let i=0; i<req.params.routeName.length; i++){
      if(req.params.routeName[i] === ' '){
        throw "NOT VALID!";
      }
    }
    if(req.params.routeName.search('UNION') !== -1 || req.params.routeName.search('union') !== -1 || req.params.routeName.search('JOIN') !== -1 || req.params.routeName.search('join') !== -1 || req.params.routeName.search('WHERE') !== -1 || req.params.routeName.search('where') !== -1 || req.params.routeName.search('ON') !== -1 || req.params.routeName.search('on') !== -1 || req.params.routeName.search('DROP') !== -1 || req.params.routeName.search('drop') !== -1 || req.params.routeName.search('SELECT') !== -1 || req.params.routeName.search('select') !== -1 || req.params.routeName.search('TRIGGER') !== -1 || req.params.routeName.search('trigger') !== -1 || req.params.routeName.search('CREATE') !== -1 || req.params.routeName.search('create') !== -1 || req.params.routeName.search('\\(') !== -1 || req.params.routeName.search('\\)') !== -1 || req.params.routeName.search(';') !== -1 || req.params.routeName.search('=') !== -1){
      throw "NOT VALID!"
    }
    sql.query(`SELECT * FROM ${req.params.routeName};`, (error, results, fields)=>{
        if(error){
            return res.status(404).send(error);
        }else{
            return res.status(200).json(results);
        }
    })
});

// Server listening setup
app.listen(3000, ()=>{
    console.log('Server is listening on port 3000');
});
