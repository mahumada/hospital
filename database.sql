DROP TABLE IF EXISTS patient, doctor, field, zone, room, operating_room, works_with;

CREATE TABLE patient (
  patient_id INT PRIMARY KEY,
  first_name VARCHAR(30) NOT NULL,
  last_name VARCHAR(30) NOT NULL,
  start_date DATETIME,
  needs VARCHAR(200),
  is_contagious BOOL,
  room_number INT
);

CREATE TABLE doctor (
  doctor_id INT PRIMARY KEY,
  first_name VARCHAR(30) NOT NULL,
  last_name VARCHAR(30) NOT NULL,
  field INT,
  salary INT,
  shift VARCHAR(50),
  start_date DATE
);

CREATE TABLE zone (
  zone_id INT PRIMARY KEY,
  zone_name VARCHAR(40) NOT NULL
);

CREATE TABLE field (
  field_id INT PRIMARY KEY,
  field_name VARCHAR(30) NOT NULL,
  budget INT,
  zone INT,
  attending INT,
  FOREIGN KEY(attending) REFERENCES doctor(doctor_id) ON DELETE SET NULL,
  FOREIGN KEY(zone) REFERENCES zone(zone_id) ON DELETE SET NULL
);

ALTER TABLE doctor ADD FOREIGN KEY(field) REFERENCES field(field_id);

CREATE TABLE room (
  room_id INT PRIMARY KEY,
  zone INT,
  is_free BOOL,
  FOREIGN KEY(zone) REFERENCES zone(zone_id) ON DELETE SET NULL
);

ALTER TABLE patient ADD FOREIGN KEY(room_number) REFERENCES room(room_id) ON DELETE SET NULL;

CREATE TABLE operating_room (
  operating_room_id INT PRIMARY KEY,
  is_free BOOL,
  zone INT,
  doctor_working INT,
  FOREIGN KEY(zone) REFERENCES zone(zone_id) ON DELETE SET NULL,
  FOREIGN KEY(doctor_working) REFERENCES doctor(doctor_id) ON DELETE SET NULL
);

CREATE TABLE works_with (
  patient_id INT,
  doctor_id INT,
  treatment VARCHAR(100),
  PRIMARY KEY(patient_id, doctor_id),
  FOREIGN KEY(patient_id) REFERENCES patient(patient_id) ON DELETE CASCADE,
  FOREIGN KEY(doctor_id) REFERENCES doctor(doctor_id) ON DELETE CASCADE
);
