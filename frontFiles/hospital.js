console.log(new Date().toISOString().slice(0, 10)+"T"+new Date().toLocaleTimeString('en-GB'));


// POST Form handler
document.querySelector('#post').addEventListener('submit', (e) => {
  e.preventDefault();
  const data = Object.fromEntries(new FormData(e.target).entries());
  const { patient_id, first_name, last_name, contagious, room_number, needs } = data;
  let is_contagious = 0;
  if(contagious === undefined){
    is_contagious = 0;
  } else {
    is_contagious = 1;
  }
  const start_date = new Date().toISOString().slice(0, 10)+"T"+new Date().toLocaleTimeString('en-GB').toString();
  fetch('/patient', {
    method: 'POST',
    body: JSON.stringify({ patient_id, first_name, last_name, start_date, needs, is_contagious, room_number }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => console.error(err))
  .then(res => console.log(res));

});


// DELETE Form handler
document.querySelector('#delete').addEventListener('submit', (e) => {
  e.preventDefault();
  const data = Object.fromEntries(new FormData(e.target).entries());
  const { patient_id } = data;
  
  fetch(`/patient/${patient_id}`, {
    method: 'DELETE'
  })
  .then(res => res.json())
  .catch(err => console.error(err))
  .then(res => console.log(res));

});


// Find From handler
document.querySelector('#findId').addEventListener('submit', (e) => {
  e.preventDefault();
  const data = Object.fromEntries(new FormData(e.target).entries());
  const { first_name, last_name } = data;
  fetch('/finder', {
    method: 'POST',
    body: JSON.stringify({ first_name, last_name }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .catch(err => document.getElementById('idNum').innerHTML = err)
  .then(res => document.getElementById('idNum').innerHTML = 'ID is ' + res[0].patient_id);
});